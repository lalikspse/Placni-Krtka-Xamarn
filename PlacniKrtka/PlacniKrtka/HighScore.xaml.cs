﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlacniKrtka
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HighScore : ContentPage
    {
        public ObservableCollection<OsobaSkore> HajSkore;
        private List<OsobaSkore> pomList;

        public HighScore(bool zadavani, int pom)
        {
            pomList = new List<OsobaSkore>();
            HajSkore = new ObservableCollection<OsobaSkore>();

            if (zadavani)
            {
                UlozScore(pom);
            }

            NactiScore();
            InitializeComponent();
            ListViewSkore.ItemsSource = HajSkore;


        }


        async private void UlozScore(int skore)
        {
            string result = await DisplayPromptAsync("HIGHSCORE", "Jméno: ", "OK", "NE");
            await App.Database.SavePersonAsync(new OsobaSkore(result, skore));
            NactiScore();
        }

        async private void NactiScore()
        {
            pomList = await App.Database.GetPeopleAsync();
            if (pomList.Count > 0)
            {
                List<OsobaSkore> pompomList = pomList.OrderByDescending(o => o.Skore).ToList();
                HajSkore.Clear();
                foreach (OsobaSkore osobaSkore in pompomList)
                {
                    HajSkore.Add(osobaSkore);
                }

            }
            else
            {

            }

        }
    }
}