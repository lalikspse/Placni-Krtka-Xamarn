﻿using SQLite;

namespace PlacniKrtka
{
    public class OsobaSkore
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Jmeno { get; set; }
        public int Skore { get; set; }

        public OsobaSkore(string jmeno, int skore)
        {
            Jmeno = jmeno;
            Skore = skore;
        }
        public OsobaSkore()
        {

        }


    }
}
