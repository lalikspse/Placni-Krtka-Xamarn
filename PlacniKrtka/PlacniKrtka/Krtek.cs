﻿using System;
using Xamarin.Forms;

namespace PlacniKrtka
{
    class Krtek
    {
        private Random random;
        public int ID { get; set; }
        public int PozX { get; set; }
        public int PozY { get; set; }
        public string zdroj { get; set; }
        public bool MuzuZabit { get; set; }
        public ImageButton imageButton { get; set; }
        public int Zivot { get; set; }
        public int Bounty { get; set; }

        public Krtek(int id, EventHandler funkce, int obtiznost)
        {

            ID = id;
            random = new Random();
            PozX = random.Next(4);
            PozY = (random.Next(4) + 1);

            Bounty = obtiznost;
            if (random.Next(10) == 5)
            {
                zdroj = "nekrtek.jpg";
                MuzuZabit = false;
            }
            else
            {
                switch (obtiznost)
                {
                    case 0:
                    case 1: zdroj = "krtek.jpg"; Zivot = 1; break;
                    case 2:
                    case 3:
                    case 4: zdroj = "krtek2.jpg"; Zivot = 2; break;
                    case 5:
                    case 6:
                    case 7: zdroj = "krtek3.jpg"; Zivot = 3; break;
                    case 8:
                    case 9:
                    case 10: zdroj = "krtek4.jpg"; Zivot = 4; break;

                }

                MuzuZabit = true;
            }

            imageButton = new ImageButton()
            {
                Source = zdroj,
                BackgroundColor = Color.Transparent,
                ClassId = id.ToString()
            };
            imageButton.Clicked += funkce;


        }

        public void UberZivot(int utok)
        {
            Zivot -= utok;
            if (MuzuZabit)
            {
                switch (Zivot)
                {
                    case 5: imageButton.Source = "krtek5.jpg"; break;
                    case 4: imageButton.Source = "krtek4.jpg"; break;
                    case 3: imageButton.Source = "krtek3.jpg"; break;
                    case 2: imageButton.Source = "krtek2.jpg"; break;
                    case 1: imageButton.Source = "krtek.jpg"; break;
                    default: break;
                }
            }

        }


    }
}
