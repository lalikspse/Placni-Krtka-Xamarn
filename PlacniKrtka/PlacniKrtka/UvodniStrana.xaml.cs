﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlacniKrtka
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UvodniStrana : ContentPage
    {
        public UvodniStrana()
        {
            InitializeComponent();
        }

        async void PlayButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());

        }

        async void HighButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new HighScore(false, 0));
        }

    }
}